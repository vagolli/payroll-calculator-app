﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Payroll.DAL;
using Payroll.Entities.Models;
using Payroll.Logic.Initializations;
using Payroll.Logic.Calculations;
using Microsoft.AspNet.Identity;
using Payroll.Entities.ViewModels;
using Payroll.Entities;
using System.Web.Script.Serialization;
using Payroll.UI.Models;
using Payroll.Logger.Common;

namespace Payroll.UI.Controllers
{
    public class MonthsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Months
        public ActionResult Index()
        {
            //try
            //{
            //    throw new Exception();
            //}
            //catch (Exception ex)
            //{
            //    StaticLogger.LogWarn(ex, "instance warn");
            //    StaticLogger.LogError(ex, "instance error");
            //    StaticLogger.LogFatal(ex, "instance fatal");
            //}

            var date = new SelectMonthVM();
            date.DateTime = DateTime.Now.ToShortDateString();

            return View(date);
        }

        // GET: Months/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Month month = db.Months.Find(id);
            if (month == null)
            {
                return HttpNotFound();
            }
            return View(month);
        }

        // GET: Months/Create
        [HttpGet]
        public ActionResult Create([Bind(Include = "DateTime")] SelectMonthVM SelectMonthVM)
        {
            var initialInput= new InputData();
            ViewData["Currency"] = initialInput.Currency();
            ViewData["LeaveDay"] = initialInput.LeaveTypes();

            MonthInitialization newMonth = new MonthInitialization();
            var Month = newMonth.MonthConstructor(Convert.ToDateTime(SelectMonthVM.DateTime), User.Identity.GetUserId());

            var salaryMonth = AutoMapper.Mapper.Map<SalaryMonthViewModel>(Month);
            return View(salaryMonth);
        }

        // POST: Months/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,Name,TotalHours,OvertimeMonthHours,TotalNationalHolidays,TotalMaternityLeaves,TotalMedicalLeaves,TotalUnpaidLeaves,Weeks,Salary,Currency")] SalaryMonthViewModel inputSalaryMonthData)
        {
            if (ModelState.IsValid)
            {
                //Hours and Salary Calc
                var userMonth = new MonthCalc();
                userMonth.TimePeriodCalculations(inputSalaryMonthData);

                var SalaryMonth = AutoMapper.Mapper.Map<Salary>(inputSalaryMonthData);
                userMonth.SalaryCalculations(SalaryMonth);
                //if (!String.IsNullOrEmpty(User.Identity.GetUserId()))  //TODO uncomment after tests on save month
                //userMonth.InsertIntoDb(SalaryMonth);

                return RedirectToAction("Index");
            }

            return View(inputSalaryMonthData);
        }

        // GET: Months/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Month month = db.Months.Find(id);
            if (month == null)
            {
                return HttpNotFound();
            }
            return View(month);
        }

        // POST: Months/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,Name,TotalHours,OvertimeMonthHours,TotalNationalHolidays,TotalMaternityLeaves,TotalMedicalLeaves,TotalUnpaidLeaves")] Month month)
        {
            if (ModelState.IsValid)
            {
                db.Entry(month).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(month);
        }

        // GET: Months/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Month month = db.Months.Find(id);
            if (month == null)
            {
                return HttpNotFound();
            }
            return View(month);
        }

        // POST: Months/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Month month = db.Months.Find(id);
            db.Months.Remove(month);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
