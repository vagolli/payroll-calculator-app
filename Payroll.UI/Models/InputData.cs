﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Payroll.UI.Models
{
    internal class InputData
    {
        internal List<SelectListItem> Currency()
        {
            List<SelectListItem> Currency = new List<SelectListItem>();
            Currency.Add(new SelectListItem { Text = "Euro", Value = "Euro" });
            Currency.Add(new SelectListItem { Text = "Leke", Value = "Leke" });

            return Currency;
        }

        internal List<SelectListItem> LeaveTypes()
        {
            List<SelectListItem> Leaves = new List<SelectListItem>();
            Leaves.Add(new SelectListItem { Text = "Select Leave", Value = "0" });
            Leaves.Add(new SelectListItem { Text = "Medical Leave", Value = "1" });
            Leaves.Add(new SelectListItem { Text = "Unpdaid Leave", Value = "1" });
            Leaves.Add(new SelectListItem { Text = "Maternity Leave", Value = "1" });

            return Leaves;
        }
    }
}
