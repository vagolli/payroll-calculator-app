﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Payroll.UI.Startup))]
namespace Payroll.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
