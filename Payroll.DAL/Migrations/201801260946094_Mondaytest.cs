namespace Payroll.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mondaytest : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Months", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Months", "Name");
        }
    }
}
