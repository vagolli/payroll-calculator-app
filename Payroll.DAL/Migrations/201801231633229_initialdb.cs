namespace Payroll.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialdb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Days",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DayOfWeek = c.String(nullable: false, maxLength: 250),
                        Month_Id = c.Int(),
                        Week_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Months", t => t.Month_Id)
                .ForeignKey("dbo.Weeks", t => t.Week_Id)
                .Index(t => t.Month_Id)
                .Index(t => t.Week_Id);
            
            CreateTable(
                "dbo.Months",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MonthYear = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Weeks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalWeekHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Month_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Months", t => t.Month_Id)
                .Index(t => t.Month_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Weeks", "Month_Id", "dbo.Months");
            DropForeignKey("dbo.Days", "Week_Id", "dbo.Weeks");
            DropForeignKey("dbo.Days", "Month_Id", "dbo.Months");
            DropIndex("dbo.Weeks", new[] { "Month_Id" });
            DropIndex("dbo.Days", new[] { "Week_Id" });
            DropIndex("dbo.Days", new[] { "Month_Id" });
            DropTable("dbo.Weeks");
            DropTable("dbo.Months");
            DropTable("dbo.Days");
        }
    }
}
