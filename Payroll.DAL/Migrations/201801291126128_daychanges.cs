namespace Payroll.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class daychanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Months", "Salary", c => c.Double());
            AddColumn("dbo.Months", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Months", "Discriminator");
            DropColumn("dbo.Months", "Salary");
        }
    }
}
