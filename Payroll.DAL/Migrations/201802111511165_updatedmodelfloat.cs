namespace Payroll.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedmodelfloat : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Days", "FirstShift", c => c.Decimal(nullable: false, precision: 18, scale: 2,defaultValue: 0));
            AlterColumn("dbo.Days", "SecondShift", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Days", "ThirdShift", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Days", "FirstShiftOvertime", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Days", "SecondShiftOvertime", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Days", "ThirdShiftOvertime", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Days", "DayOvertimeHours", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Months", "TotalHours", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Months", "OvertimeMonthHours", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Weeks", "TotalWeekHours", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
            AlterColumn("dbo.Weeks", "OvertimeWeekHours", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue: 0));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Weeks", "OvertimeWeekHours", c => c.Int(nullable: false));
            AlterColumn("dbo.Weeks", "TotalWeekHours", c => c.Int(nullable: false));
            AlterColumn("dbo.Months", "OvertimeMonthHours", c => c.Int(nullable: false));
            AlterColumn("dbo.Months", "TotalHours", c => c.Int(nullable: false));
            AlterColumn("dbo.Days", "DayOvertimeHours", c => c.Int(nullable: false));
            AlterColumn("dbo.Days", "ThirdShiftOvertime", c => c.Int(nullable: false));
            AlterColumn("dbo.Days", "SecondShiftOvertime", c => c.Int(nullable: false));
            AlterColumn("dbo.Days", "FirstShiftOvertime", c => c.Int(nullable: false));
            AlterColumn("dbo.Days", "ThirdShift", c => c.Int(nullable: false));
            AlterColumn("dbo.Days", "SecondShift", c => c.Int(nullable: false));
            AlterColumn("dbo.Days", "FirstShift", c => c.Int(nullable: false));
        }
    }
}
