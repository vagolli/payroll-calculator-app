namespace Payroll.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Months", "GrossInputSalary", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "GrossSalary", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "NetSalary", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "SalaryForContributionsPurposes", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "SocialSecurityContributions", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "EmployerSocialSecurityContributions", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "EmployeeSocialSecurityContributions", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "HealthSecurityContributions", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "PIT", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Months", "Date", c => c.String());
            DropColumn("dbo.Months", "Salary");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Months", "Salary", c => c.Double());
            DropColumn("dbo.Months", "Date");
            DropColumn("dbo.Months", "PIT");
            DropColumn("dbo.Months", "HealthSecurityContributions");
            DropColumn("dbo.Months", "EmployeeSocialSecurityContributions");
            DropColumn("dbo.Months", "EmployerSocialSecurityContributions");
            DropColumn("dbo.Months", "SocialSecurityContributions");
            DropColumn("dbo.Months", "SalaryForContributionsPurposes");
            DropColumn("dbo.Months", "NetSalary");
            DropColumn("dbo.Months", "GrossSalary");
            DropColumn("dbo.Months", "GrossInputSalary");
        }
    }
}
