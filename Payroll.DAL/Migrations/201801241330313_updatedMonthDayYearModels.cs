namespace Payroll.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedMonthDayYearModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Days", "FirstShift", c => c.Int(nullable: false));
            AddColumn("dbo.Days", "SecondShift", c => c.Int(nullable: false));
            AddColumn("dbo.Days", "ThirdShift", c => c.Int(nullable: false));
            AddColumn("dbo.Days", "FirstShiftOvertime", c => c.Int(nullable: false));
            AddColumn("dbo.Days", "SecondShiftOvertime", c => c.Int(nullable: false));
            AddColumn("dbo.Days", "ThirdShiftOvertime", c => c.Int(nullable: false));
            AddColumn("dbo.Days", "DayOvertimeHours", c => c.Int(nullable: false));
            AddColumn("dbo.Days", "IsNationalHoliday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Days", "IsMaternityLeave", c => c.Boolean(nullable: false));
            AddColumn("dbo.Days", "IsMedicalLeave", c => c.Boolean(nullable: false));
            AddColumn("dbo.Days", "isUnpdaidLeave", c => c.Boolean(nullable: false));
            AddColumn("dbo.Days", "Date", c => c.String());
            AddColumn("dbo.Months", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.Months", "TotalHours", c => c.Int(nullable: false));
            AddColumn("dbo.Months", "OvertimeMonthHours", c => c.Int(nullable: false));
            AddColumn("dbo.Months", "TotalNationalHolidays", c => c.Int(nullable: false));
            AddColumn("dbo.Months", "TotalMaternityLeaves", c => c.Int(nullable: false));
            AddColumn("dbo.Months", "TotalMedicalLeaves", c => c.Int(nullable: false));
            AddColumn("dbo.Months", "TotalUnpaidLeaves", c => c.Int(nullable: false));
            AddColumn("dbo.Weeks", "OvertimeWeekHours", c => c.Int(nullable: false));
            AlterColumn("dbo.Weeks", "TotalWeekHours", c => c.Int(nullable: false));
            DropColumn("dbo.Months", "MonthYear");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Months", "MonthYear", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Weeks", "TotalWeekHours", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Weeks", "OvertimeWeekHours");
            DropColumn("dbo.Months", "TotalUnpaidLeaves");
            DropColumn("dbo.Months", "TotalMedicalLeaves");
            DropColumn("dbo.Months", "TotalMaternityLeaves");
            DropColumn("dbo.Months", "TotalNationalHolidays");
            DropColumn("dbo.Months", "OvertimeMonthHours");
            DropColumn("dbo.Months", "TotalHours");
            DropColumn("dbo.Months", "UserId");
            DropColumn("dbo.Days", "Date");
            DropColumn("dbo.Days", "isUnpdaidLeave");
            DropColumn("dbo.Days", "IsMedicalLeave");
            DropColumn("dbo.Days", "IsMaternityLeave");
            DropColumn("dbo.Days", "IsNationalHoliday");
            DropColumn("dbo.Days", "DayOvertimeHours");
            DropColumn("dbo.Days", "ThirdShiftOvertime");
            DropColumn("dbo.Days", "SecondShiftOvertime");
            DropColumn("dbo.Days", "FirstShiftOvertime");
            DropColumn("dbo.Days", "ThirdShift");
            DropColumn("dbo.Days", "SecondShift");
            DropColumn("dbo.Days", "FirstShift");
        }
    }
}
