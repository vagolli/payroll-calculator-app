namespace Payroll.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCurrecncy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Months", "Currency", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Months", "Currency");
        }
    }
}
