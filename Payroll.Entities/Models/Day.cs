﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Entities.Models
{
    public class Day
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(250, MinimumLength = 1)]
        [Display(Name = "Item Name")]
        public string DayOfWeek{ get; set; }

        [Display(Name = "06' - 19'")]
        [Range(0, 13, ErrorMessage = "Please enter max 13 hours")]
        public decimal FirstShift { get; set; }

        [Display(Name = "19' - 22'")]
        [Range(0, 3, ErrorMessage = "Please enter max 3 hours")]
        public decimal SecondShift { get; set; }

        [Display(Name = "22' - 06'")]
        [Range(0, 16, ErrorMessage = "Please enter max 8 hours")]
        public decimal ThirdShift { get; set; }

        [Display(Name = "First Shift Overtimes")]
        public decimal FirstShiftOvertime { get; set; }

        [Display(Name = "Second Shift Overtimes")]
        public decimal SecondShiftOvertime { get; set; }

        [Display(Name = "Third Shift Overtimes")]
        public decimal ThirdShiftOvertime { get; set; }

        [Display(Name = "Day Overtimes")]
        public decimal DayOvertimeHours { get; set; }

        [Display(Name = "National Holiday")]
        public bool IsNationalHoliday { get; set; }

        [Display(Name = "Maternity Leave")]
        public bool IsMaternityLeave { get; set; }

        [Display(Name = "Medical Leave")]
        public bool IsMedicalLeave { get; set; }

        [Display(Name = "Unpaid Leave")]
        public bool isUnpdaidLeave { get; set; }

        public string Date { get; set; }

    }
}
