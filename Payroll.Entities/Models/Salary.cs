﻿using Payroll.Entities.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Entities.Models
{
    public class Salary : SalaryMonthViewModel
    {
        [Range(0, 999999999)]
        public decimal GrossSalary { get; set; }

        [Range(0, 999999999)]
        public decimal NetSalary { get; set; }

        [Range(0, 999999999)]
        public decimal SalaryForContributionsPurposes { get; set; }

        [Range(0, 999999999)]
        public decimal SocialSecurityContributions { get; set; }

        [Range(0, 999999999)]
        public decimal EmployerSocialSecurityContributions { get; set; }

        [Range(0, 999999999)]
        public decimal EmployeeSocialSecurityContributions { get; set; }

        [Range(0, 999999999)]
        public decimal HealthSecurityContributions { get; set; }

        [Range(0, 999999999)]
        public decimal PIT { get; set; }

        public string Date { get; set; }
    }
}
