﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Entities.Models
{
    public class Month
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "User")]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Month Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Total Hours")]
        public decimal TotalHours { get; set; }

        [Required]
        [Display(Name = "Overtime Month Hours")]
        public decimal OvertimeMonthHours { get; set; }

        [Required]
        [Display(Name = "Total National Holidays")]
        public int TotalNationalHolidays { get; set; }

        [Required]
        [Display(Name = "Total Maternity Leaves")]
        public int TotalMaternityLeaves { get; set; }

        [Required]
        [Display(Name = "Total Medical Leaves")]
        public int TotalMedicalLeaves { get; set; }

        [Required]
        [Display(Name = "Total Unpaid Leaves")]
        public int TotalUnpaidLeaves { get; set; }

        public IList<Week> Weeks { get; set; }
        public IList<Day> Days { get; set; }
    }
}
