﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Entities.Models
{
    public class NationalHolidays
    {
        public Dictionary<DateTime, string> Holidays()
        {
            Dictionary<DateTime, string> NationalHolidays = new Dictionary<DateTime, string>();

            NationalHolidays.Add(new DateTime(2018, 1, 1),"Viti i Ri");
            NationalHolidays.Add(new DateTime(2018, 1, 2),"Viti i Ri");
            NationalHolidays.Add(new DateTime(2018, 3, 14),"Dita e Veres");
            NationalHolidays.Add(new DateTime(2018, 3, 22),"Dita e Novruzit");
            NationalHolidays.Add(new DateTime(2018, 4, 1),"E diela e Pashkeve Katolike");
            NationalHolidays.Add(new DateTime(2018, 4, 08),"E diela e Pashkeve Ortodokse");
            NationalHolidays.Add(new DateTime(2018, 5, 1),"Dita Nderkombetare e Punetoreve");
            NationalHolidays.Add(new DateTime(2018, 6, 15),"Dita e Bajramit te Madh");
            NationalHolidays.Add(new DateTime(2018, 8, 21),"Dita e Kurban Bajramit te Madh");
            NationalHolidays.Add(new DateTime(2018, 10, 19),"Dita e Lumturimit te Nene Terezes");
            NationalHolidays.Add(new DateTime(2018, 11, 28),"Dita e Flamurit dhe Pavarsise");
            NationalHolidays.Add(new DateTime(2018, 11, 29),"Dita e Clirimit");
            NationalHolidays.Add(new DateTime(2018, 12, 08),"Dita Kombetare e Rinise");
            NationalHolidays.Add(new DateTime(2018, 12, 25),"Krishtlindjet");

            return NationalHolidays;
        }
    }
}
