﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Entities.Models
{
    public class Week
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Total Week Hours")]
        public decimal TotalWeekHours { get; set; }

        [Display(Name = "Overtime Week Hours")]
        public decimal OvertimeWeekHours { get; set; }

        public IList<Day> Days { get; set; }
    }
}
