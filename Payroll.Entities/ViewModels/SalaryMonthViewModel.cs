﻿using Payroll.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Entities.ViewModels
{
    public class SalaryMonthViewModel : Month
    {
        [Display(Name="Gross input salary")]
        public decimal GrossInputSalary { get; set; }

        [Display(Name = "Euro/Leke")]
        public string Currency { get; set; }
    }
}
