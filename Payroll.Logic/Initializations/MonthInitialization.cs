﻿using Payroll.DAL;
using Payroll.Entities.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;


namespace Payroll.Logic.Initializations
{
   public class MonthInitialization
    {
        public Month MonthConstructor(DateTime selectedDate, string userId)
        {
            var WeeksData = WeekDays(selectedDate); //weeks and day dates data

            Month month = new Month(); //empty month

            if (!String.IsNullOrEmpty(userId))
                month.UserId = Guid.NewGuid().ToString();
            else
                month.UserId = "User is not authenticated";

            month.Name = selectedDate.ToString("MMM , yyyy");

            var listDaysInMonth = new List<Day>();
            var listWeeksInMonth = new List<Week>();

            var FestiveDays = new NationalHolidays().Holidays();

            foreach (var week in WeeksData)
            {
                var weekofMonth = new Week(); //week in month
                var listDaysInWeek = new List<Day>(); //list of days for each week

                foreach (var dateofDay in week)
                {
                    //Constructing Day
                    var dayofWeek = new Day();
                    dayofWeek.Date = dateofDay.Date.ToString("dd,MM,yyyy");
                    dayofWeek.DayOfWeek = dateofDay.DayOfWeek.ToString();

                    if (FestiveDays.ContainsKey(Convert.ToDateTime(dayofWeek.Date)))
                        dayofWeek.IsNationalHoliday = true;

                    listDaysInWeek.Add(dayofWeek);
                    listDaysInMonth.Add(dayofWeek);
                }
                //Constructing Week
                weekofMonth.Days = listDaysInWeek;
                listWeeksInMonth.Add(weekofMonth);
            }
            month.Weeks = listWeeksInMonth;
            month.Days = listDaysInMonth;

            return month;
        }

        //Returns each week of month with its day dates
        private List<List<DateTime>> WeekDays(DateTime selectedDate)
        {
            Calendar calendar = CultureInfo.CurrentCulture.Calendar;

            IEnumerable<int> daysInMonth = Enumerable.Range(1, calendar.GetDaysInMonth(selectedDate.Year, selectedDate.Month));

            List<List<DateTime>> daysForEachWeek = daysInMonth.Select(day => new DateTime(selectedDate.Year, selectedDate.Month, day))
                .GroupBy(d => calendar.GetWeekOfYear(d, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday))
                .Select(g => new List<DateTime>(g))
                .ToList();
            
            return daysForEachWeek;
        }
    }
}
