﻿using Payroll.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Logic.Initializations
{
   public static class DateTimeHelpers
    {
        //returns the number of weeks in a month 
        public static int WeeksInMonth(this DateTime Date)
        {
            int numberOfWeeks = 0;
            int daysThisMonth = DateTime.DaysInMonth(Date.Year, Date.Month);
            DateTime firstDayOfMonth = new DateTime(Date.Year, Date.Month, 1);

            switch (firstDayOfMonth.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    for (int i = 0; i < daysThisMonth; i++)
                        if (firstDayOfMonth.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                            numberOfWeeks++;
                    break;
                default:
                    for (int i = 0; i < daysThisMonth; i++)
                    {
                        if (firstDayOfMonth.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                            numberOfWeeks++;
                    }
                    numberOfWeeks++;
                    break;
            }
            return numberOfWeeks;
        }

        //reutrns true if its last day of the month
        public static bool IsLastDayOfTheMonth(this DateTime dateTime)
        {
            return dateTime == new DateTime(dateTime.Year, dateTime.Month, 1).AddMonths(1).AddDays(-1);
        }
    }
}
