﻿using Payroll.DAL;
using Payroll.Entities.Models;
using System;
using System.Collections.Generic;

namespace Payroll.Logic.Calculations
{
    public class MonthCalc
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void TimePeriodCalculations(Month month)
        {
            var daysOfMonth = new List<Day>();
            foreach (var week in month.Weeks)
            {
                foreach (var day in week.Days)
                {
                    daysOfMonth.Add(day);
                }
            }
            month.Days = daysOfMonth;
        }

        public void SalaryCalculations(Salary inputedmonth)
        {
            inputedmonth.Date = DateTime.Now.ToShortDateString();

            var MonthData = HoursOvertimesLeaves(inputedmonth);
            decimal T1Month = MonthData.T1TotMonth;
            decimal T2Month = MonthData.T2TotMonth;
            decimal T3Month = MonthData.T3TotMonth;
            Salary month = MonthData.salaryData;

            
            SalaryPay(month, T1Month, T2Month, T3Month); //TODO
        }

        private void SalaryPay(Salary monthSalary, decimal T1TotMonth, decimal T2TotMonth, decimal T3TotMonth) //TODO Extend salaray calculation // nett gross
        {
            decimal GrossInputSalary = monthSalary.GrossInputSalary;
            decimal TotalHours = monthSalary.TotalHours;
            decimal TotalOvertimes = monthSalary.OvertimeMonthHours;

            int maternityLeave = monthSalary.TotalMaternityLeaves;
            int nationalLeave = monthSalary.TotalNationalHolidays;
            int medicalLeave = monthSalary.TotalMedicalLeaves;
            
        }

        public (decimal T1TotMonth, decimal T2TotMonth, decimal T3TotMonth, Salary salaryData) HoursOvertimesLeaves(Salary month) // tuple return type
        {
            var daysOfMonth = new List<Day>();

            decimal T1Month = 0;
            decimal T2Month = 0;
            decimal T3Month = 0;

            foreach (var week in month.Weeks)
            {
                foreach (var day in week.Days)
                {
                    decimal totDayHours = 0;

                    //Days Hours and Leaves
                    if (day.IsMaternityLeave)
                    {
                        month.TotalMaternityLeaves++;
                        day.FirstShift = 6;
                        T1Month = T1Month + day.FirstShift;
                    }
                    else if (day.IsMedicalLeave)
                    {
                        month.TotalMedicalLeaves++;
                        day.FirstShift = 6;
                        T1Month = T1Month + day.FirstShift;
                    }
                    else if (day.isUnpdaidLeave)
                    {
                        day.FirstShift = 0;
                        month.TotalUnpaidLeaves++;
                    }
                    else if (day.IsNationalHoliday)
                    {
                        day.FirstShift = day.FirstShift * (decimal)1.25;
                        day.SecondShift = day.FirstShift * (decimal)1.25;
                        day.ThirdShift = day.FirstShift * (decimal)1.25;
                        month.TotalNationalHolidays++;
                        T1Month = T1Month + day.FirstShift;
                    }
                    else
                    {
                        T1Month = T1Month + day.FirstShift;
                        T2Month = T2Month + day.SecondShift;
                        T3Month = T3Month + day.ThirdShift;
                    }

                    totDayHours = totDayHours + day.FirstShift + day.SecondShift + day.ThirdShift;

                    //Day Overtimes, week total hours, month overtimes
                    if (totDayHours > 8)
                    {
                        day.DayOvertimeHours = totDayHours - 8;
                        if (T1Month != 0 & T1Month > day.DayOvertimeHours)
                            T1Month = T1Month - day.DayOvertimeHours;
                        else if (T2Month != 0 & T2Month > day.DayOvertimeHours)
                            T2Month = T2Month - day.DayOvertimeHours;
                        else if (T2Month != 0 & T2Month > day.DayOvertimeHours)
                            T3Month = T3Month - day.DayOvertimeHours;
                    }
                    totDayHours = day.FirstShift + day.SecondShift + day.ThirdShift;
                    week.TotalWeekHours = week.TotalWeekHours + totDayHours;
                    month.TotalHours = month.TotalHours + totDayHours;
                    month.OvertimeMonthHours = month.OvertimeMonthHours + day.DayOvertimeHours;

                    daysOfMonth.Add(day);
                }

                //Week & Month Overtimes
                if (week.TotalWeekHours > 40)
                {
                    week.OvertimeWeekHours = week.TotalWeekHours - 40;
                    month.OvertimeMonthHours = month.OvertimeMonthHours + week.OvertimeWeekHours;
                }
            }
            month.Days = daysOfMonth;
            
            return (T1Month, T2Month, T3Month, month);
        }


        #region    InsertIntoDb

        public void InsertIntoDb(Salary Salary) //saving salary into db
        {
            using (db)
            {
                foreach (var week in Salary.Weeks)
                {
                    foreach (var day in week.Days)
                    {
                        db.Days.Add(day);
                    }
                    db.Weeks.Add(week);
                }
                db.Months.Add(Salary);
                db.SaveChanges();
            }
        }

        public void InsertIntoDb(Month month) //saving month into db
        {
            using (db)
            {
                foreach (var week in month.Weeks)
                {
                    foreach (var day in week.Days)
                    {
                        db.Days.Add(day);
                    }
                    db.Weeks.Add(week);
                }
                db.Months.Add(month);
                db.SaveChanges();
            }
        }

        #endregion

    }
}
