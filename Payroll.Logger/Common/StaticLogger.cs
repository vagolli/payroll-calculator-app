﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Payroll.Logger.Common
{
    public static class StaticLogger
    {
        public static void LogWarn(Exception ex, string text)
        {
            NLog.Logger logger = LogManager.GetLogger("databaseLogger");
            logger.Warn(ex, text);
        }

        public static void LogError(Exception ex, string text)
        {
            NLog.Logger logger = LogManager.GetLogger("databaseLogger");
            logger.Error(ex, text);
        }

        public static void LogFatal(Exception ex, string text)
        {
            NLog.Logger logger = LogManager.GetLogger("databaseLogger");
            logger.Fatal(ex, text);
        }
    }
}
